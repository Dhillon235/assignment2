﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace prog_milestone2_team11
{
    class Program
    {
        static void Main(string[] args)
        {
            int a = 0;
            var papercode5 = new string[4] { " ", " ", " ", " " };
            var papercode6 = new string[3] { " ", " ", " " };
            var marks5 = new double[4] { 0.0, 0.0, 0.0, 0.0 };
            var marks6 = new double[3] { 0.0, 0.0, 0.0 };

            int studID = 0;

            Console.WriteLine("Are you in level 5 or 6");
            a = int.Parse(Console.ReadLine());

            if (a == 5)
            {
                Console.WriteLine(PaperCode5(a, papercode5, marks5, studID));
            }
            else
            {
                Console.WriteLine(PaperCode6(a, papercode6, marks6, studID));
            }
        }
        static string PaperCode5(int a, string[] papercode5, double[] marks5, int studID)
        {
            Console.WriteLine("Enter the first paper code");
            papercode5[0] = Console.ReadLine();

            Console.WriteLine("Enter the marks for that paper");
            marks5[0] = double.Parse(Console.ReadLine());

            Console.WriteLine("Enter the second paper code");
            papercode5[1] = Console.ReadLine();

            Console.WriteLine("Enter the marks for that paper");
            marks5[1] = double.Parse(Console.ReadLine());

            Console.WriteLine("Enter the third paper code");
            papercode5[2] = Console.ReadLine();

            Console.WriteLine("Enter the marks for that paper");
            marks5[2] = double.Parse(Console.ReadLine());

            Console.WriteLine("Enter the fourth paper code");
            papercode5[3] = Console.ReadLine();

            Console.WriteLine("Enter the marks for that paper");
            marks5[3] = double.Parse(Console.ReadLine());

            Console.WriteLine("Enter your student ID");
            studID = int.Parse(Console.ReadLine());

            return $"for the {papercode5[0]} you have {marks5[0]} \n {papercode5[1]}, {marks5[1]} \n {papercode5[2]}, {marks5[2]} \n {papercode5[3]}, {marks5[3]} \n {studID}";
        }
        static string PaperCode6(int a, string[] papercode6, double[] marks6, int studID)
        {
            Console.WriteLine("Enter the first paper code");
            papercode6[0] = Console.ReadLine();

            Console.WriteLine("Enter the marks for that paper");
            marks6[0] = double.Parse(Console.ReadLine());

            Console.WriteLine("Enter the second paper code");
            papercode6[1] = Console.ReadLine();

            Console.WriteLine("Enter the marks for that paper");
            marks6[1] = double.Parse(Console.ReadLine());

            Console.WriteLine("Enter the third paper code");
            papercode6[2] = Console.ReadLine();

            Console.WriteLine("Enter the marks for that paper");
            marks6[2] = double.Parse(Console.ReadLine());

            Console.WriteLine("Enter your student ID");
            studID = int.Parse(Console.ReadLine());

            return $"for the {papercode6[0]} you have {marks6[0]} \n {papercode6[1]}, {marks6[1]} \n {papercode6[2]}, {marks6[2]} \n {studID}";

        }

    //    static string PercentageOfMarks(int a, string[] papercode, double[] marks, int studID)


    }
}
